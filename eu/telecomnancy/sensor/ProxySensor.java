package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Date;

import eu.telecomnancy.ui.Vue;

public class ProxySensor implements ISensor{

	private ArrayList<String> log = new ArrayList<String>();
	private ArrayList<Vue> observer = new ArrayList<Vue>();
    private State state;
    private boolean status = false;
    double value = 0;

    @Override
    public void on(){
        state = new StateOn();
        status = true;
        
        Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [ON]");
        System.out.println(log.get(log.size() -1));
    }

    @Override
    public void off(){
    	state = new StateOff();
    	status = false;
    	
    	Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [OFF]");
        System.out.println(log.get(log.size() -1));
    }

    @Override
    public boolean getStatus(){
    	Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [getStatus] " + getStatus());
        System.out.println(log.get(log.size() -1));
        
        return status;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
        
        Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [update]");
        System.out.println(log.get(log.size() -1));
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [getValue] " +getValue());
        System.out.println(log.get(log.size() -1));
        
        return state.getValue();
    }
 
    public void notifyV() throws SensorNotActivatedException{
    	for (Vue vue : observer){
    		vue.update();
    	}
    	
    	Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [notifyV]");
        System.out.println(log.get(log.size() -1));
    }

	@Override
	public void attach(Vue vue) {
		if(vue!=null){
			observer.add(vue);
		}
		
		Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [attach]");
        System.out.println(log.get(log.size() -1));
	}

	@Override
	public void detach(Vue vue) {
		if (observer!=null){
			observer.remove(vue);
		}
		
		Date date = new Date();
        String day = date.getDay() +"/" + date.getMonth() +"/"+ date.getYear();
        log.add("["+day+"] [detach]");
        System.out.println(log.get(log.size() -1));
	}
}
