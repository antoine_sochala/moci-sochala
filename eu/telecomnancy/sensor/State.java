package eu.telecomnancy.sensor;

public interface State {
	
	public void update() throws SensorNotActivatedException;
	public double getValue() throws SensorNotActivatedException;

}
