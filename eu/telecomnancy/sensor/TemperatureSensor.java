package eu.telecomnancy.sensor;

import java.util.ArrayList;

import eu.telecomnancy.ui.Vue;

public class TemperatureSensor implements ISensor {
	private ArrayList<Vue> observer = new ArrayList<Vue>();
    private State state;
    private boolean status = false;
    double value = 0;

    @Override
    public void on(){
        state = new StateOn();
        status = true;
    }

    @Override
    public void off(){
    	state = new StateOff();
    	status = false;
    }

    @Override
    public boolean getStatus(){
        return status;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return state.getValue();
    }
 
    public void notifyV() throws SensorNotActivatedException{
    	for (Vue vue : observer){
    		vue.update();
    	}
    }

	@Override
	public void attach(Vue vue) {
		if(vue!=null){
			observer.add(vue);
		}
	}

	@Override
	public void detach(Vue vue) {
		if (observer!=null){
			observer.remove(vue);
		}
	}
}
