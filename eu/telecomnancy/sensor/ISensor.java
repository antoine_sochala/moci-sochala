package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Observable;

public interface ISensor extends Observable {
    /**
     * Enable the sensor.
     * @throws SensorNotActivatedException 
     */
    public void on();

    /**
     * Disable the sensor.
     * @throws SensorNotActivatedException 
     */
    public void off();

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     * @throws SensorNotActivatedException 
     */
    public boolean getStatus();

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException;

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException;

}
