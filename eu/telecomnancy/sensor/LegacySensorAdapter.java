package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.Vue;

public class LegacySensorAdapter implements ISensor {

	private LegacyTemperatureSensor adaptee;
	
	public LegacySensorAdapter(LegacyTemperatureSensor adaptee){
		this.adaptee=adaptee;
	}
	
	
	@Override
	public void on() {
		if(!getStatus())
			adaptee.onOff();
	}

	@Override
	public void off() {
		if(getStatus())
			adaptee.onOff();
	}

	@Override
	public boolean getStatus() {
		return adaptee.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return adaptee.getTemperature();
	}


	@Override
	public void attach(Vue vue) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void detach(Vue vue) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void notifyV() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		
	}

}
