package eu.telecomnancy.sensor;

import java.util.Random;

public class StateOn implements State {
	
	private double value = 0;

	@Override
	public void update() throws SensorNotActivatedException {
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}
	
	

}
