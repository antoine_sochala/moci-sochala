package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface Vue {
	
    public void update() throws SensorNotActivatedException;

}
