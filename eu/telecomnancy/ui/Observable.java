package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public interface Observable {
	
	public void attach(Vue vue);
	
	public void detach(Vue vue);
	
	public void notifyV() throws SensorNotActivatedException;

}
