package eu.telecomnancy;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacySensorAdapter;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class Client {
	
	public static void main(String[] args) {
		 LegacyTemperatureSensor adaptee = new LegacyTemperatureSensor();
		 ISensor adapter = new LegacySensorAdapter(adaptee);
		 new ConsoleUI(adapter);
	    }
}
