package eu.telecomnancy;



import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.ProxySensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new ProxySensor();
        new MainWindow(sensor);
    }

}
